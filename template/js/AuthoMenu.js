var buttonLogin = document.getElementById("log-in");
var buttonSignup = document.getElementById("sign-up");

var login = document.getElementById("login");
var signup = document.getElementById("signup");

var bg_layer = document.getElementById("bg_layer1");
var bg_layer2 = document.getElementById("bg_layer2");

buttonLogin.addEventListener("click",OpenMenuLogin);
bg_layer.addEventListener("click",CloseMenuLogin); 

buttonSignup.addEventListener("click",OpenMenuSignup);
bg_layer2.addEventListener("click",CloseMenuSignup);

function OpenMenuLogin(){
	bg_layer.classList.toggle("bg_layer");
	login.classList.toggle("active");
}

function CloseMenuLogin(){
	login.classList.toggle("active");
	bg_layer.classList.toggle("bg_layer");
}


function OpenMenuSignup(){
	bg_layer2.classList.toggle("bg_layer");
	signup.classList.toggle("active");
}

function CloseMenuSignup(){
	signup.classList.toggle("active");
	bg_layer2.classList.toggle("bg_layer");
}