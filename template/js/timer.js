 
function confirmExit(){
    alert("confirm exit is being called");
    return false;
}




var audio = new Audio(); 
audio.src = '/projects/pomodoro.com/template/sounds/Napalm Death - You Suffer.mp3';
//audio.autoplay = true; // Автоматически запускаем

var start = document.getElementById("start");
var stop = document.getElementById("stop");
var reset = document.getElementById("reset");

start.addEventListener("click",StartTime);
stop.addEventListener("click",StopTime);
reset.addEventListener("click",ResetTime);

const TimeWorkM =+(document.getElementById("timeWork").innerHTML);
const TimeWorkS  = 0;
const TimeBreakM = +(document.getElementById("timeBreak").innerHTML);
const TimeBreakS  = 0;
const longBreakDelay = +(document.getElementById("countTimes").innerHTML);


var countTimes=+(document.getElementById("figureTomatos").innerHTML);
var timeEnd = "";
var today = "";
var timerId = "";
var MinutesWork = TimeWorkM;
var SecondsWork = TimeWorkS;
var MinutesBreak = TimeBreakM;
var LongBreak = +(document.getElementById("longBreak").innerHTML);
var SecondsBreak = TimeBreakS;
var isTime = "TimeWork";
var time = document.getElementById("time");
var countTomatos = document.getElementById("countTomatos");

var n_tsec = 0;
var n_tmin = TimeWorkM;
var state = "stop";

function StartTime(){
	if(state == "start"){
		
	} else {
		window.onbeforeunload = confirmExit;
		state = "start";
	timeEnd = new Date();
	if(isTime=="TimeWork"){
		timeEnd.setMinutes(timeEnd.getMinutes()+MinutesWork);
		timeEnd.setSeconds(timeEnd.getSeconds()+SecondsWork);
	}else{
		timeEnd.setMinutes(timeEnd.getMinutes()+MinutesBreak);
		timeEnd.setSeconds(timeEnd.getSeconds()+SecondsBreak);
	}

	var tsec= 0;
	var tmin = 0;
	MinutesWork = TimeWorkM;
	SecondsWork = TimeWorkS;
	MinutesBreak = TimeBreakM;
	SecondsBreak = TimeBreakS;

	timerId = setInterval(function(){	
	
		today = new Date();
    	today = Math.floor((timeEnd-today)/1000);
    	n_tsec = today%60;
    	tsec = n_tsec;
    	today = Math.floor(today/60);

    	if(n_tsec<10)tsec = '0' + n_tsec;

    	n_tmin = today%MinutesWork; 
    	tmin = n_tmin;
    	today = Math.floor(today/60);
    	if(n_tmin < 10)tmin = '0'+n_tmin;

    	//thour=today%24; 
    	//today=Math.floor(today/24);
    
    	timestr = tmin+":"+tsec;

    	if(isTime=="TimeWork")
			document.title = "Pomadoro: Work";
		else
			document.title = "Pomadoro: Break";
		time.innerHTML = timestr;

		if(n_tmin<=0 && n_tsec<=0){

			timeEnd = new Date();
			if(isTime=="TimeBreak"){
				timeEnd = new Date();
				timeEnd.setMinutes(timeEnd.getMinutes()+MinutesWork);
				timeEnd.setSeconds(timeEnd.getSeconds()+SecondsWork);
				isTime = "TimeWork";
			} else{
				timeEnd = new Date();
				countTimes++;
				countTomatos.innerHTML="POMODORO #"+countTimes;
				if(countTimes%longBreakDelay==0){
					timeEnd.setMinutes(timeEnd.getMinutes()+LongBreak);
					timeEnd.setSeconds(timeEnd.getSeconds()+SecondsBreak);
				}else{
					timeEnd.setMinutes(timeEnd.getMinutes()+MinutesBreak);
					timeEnd.setSeconds(timeEnd.getSeconds()+SecondsBreak);
			}
				isTime = "TimeBreak";
				AddTimeWorkToDB(MinutesWork);
				audio.play();
			}
		}
	}, 900);
}
}

function StopTime(){
	state = "stop";
	document.title = "Pomadoro: Pause";
	if(isTime=="TimeWork"){
		MinutesWork = n_tmin;
		SecondsWork = n_tsec;
	}else{
		MinutesBreak = n_tmin;
		SecondsBreak = n_tsec;
	}
	clearInterval(timerId);
}

function ResetTime(){
	window.onbeforeunload = function(){};
	state = "reset";
	time.innerHTML = "25:00";
	MinutesWork = TimeWorkM;
	SecondsWork = 0;
	clearInterval(timerId);
	isTime = "TimeWork";
}
