<?php

class Statistic extends Model{
	
	public function getList(){
		$sql = "select date_format(date,'%d/%m/%Y %H:%i') as date,TIME_FORMAT(timework, '%i:%s') as minuteswork,TIME_FORMAT(timework, '%H') as hourswork from statistics WHERE userId=".Session::get("id")." order by date desc limit 0, 14;";

		return $this->db->query($sql);
	}

	public function getByAlias($alias){
		$alias = $this->db->escape($alias);
		$alias-=1;
		$limitStart = $alias*14;
		$sql = "select date_format(date,'%d/%m/%Y %H:%i') as date,TIME_FORMAT(timework, '%i:%s') as minuteswork,TIME_FORMAT(timework, '%H') as hourswork from statistics WHERE userId=".Session::get("id")." order by date desc limit {$limitStart}, 14;";
		$result = $this->db->query($sql);

		return isset($result) ? $result : null;
	}

	public function getCountPomodoros(){
		$sql = "select count(*) as size from statistics where userId=".Session::get("id").";";
		$result = $this->db->query($sql);
		if(isset($result[0])){
			return $result[0]['size'];
		}

		return false;
	}

	public function getStringTotalTimeWork(){
		$sql = "select sum(time_to_sec(timeWork)) as time_sec from statistics where userId=".Session::get("id").";";
		$result = $this->db->query($sql);
		
		$second = 0;
		$hours = 0;
  		$minutes = 0;
		if(isset($result[0])){
			$second = $result[0]['time_sec'];
			$hours = floor($second/3600);
  			$minutes = floor(($second-$hours*3600)/60);

  			if($hours<10){
  				$hours="0{$hours}";
  			}
  			if($minutes<10){
  				$minutes="0{$minutes}";
  			}
			
			return "{$hours}:{$minutes}:00";
		}

		return false;
	}

	public function getCountDays(){
		$sql = "select count(distinct day(date)) as days from statistics where userId=".Session::get("id")." group by month(date);";
		$result = $this->db->query($sql);
		$countDays = 0;
		if(isset($result)){
			for ($i=0; $i < count($result); $i++) { 
				$countDays += $result[$i]['days'];
			}
			return $countDays;
		}

		return false;
	}
}