<?php

class TimeSettings extends Model{

	public function getSettings(){
		$sql = "select floor(time_to_sec(timeWork) / 60) as timeWork,floor(time_to_sec(timeBreak) / 60) as timeBreak,floor(time_to_sec(longBreak) / 60) as longBreak,countTimes from settings where userId=".Session::get("id");
		$result = $this->db->query($sql);
		if(isset($result[0])){
			return $result[0];
		}

		return false;
	}
}