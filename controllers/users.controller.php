<?php

class UsersController extends Controller{

	public function __construct($data = array()){
		parent::__construct($data);
		$this->model = new User();
	}

	public function login(){
		$this->data['error'] = '';
		if( $_POST && isset($_POST['login']) && isset($_POST['password']) ){
			$user = $this->model->getByLogin($_POST['login']);
			$hash = md5(Config::get('salt').$_POST['password']);
			if($user && $hash == $user['password']){
				Session::set('login', $user['login']);
				Session::set('id', $user['id']);
				Session::set('role', 'user');
				
				setcookie('login', $user['login'], time()+60*60*24*7); //логин
				setcookie('id', $user['id'], time()+60*60*24*7);
				
				Router::redirect(PROJECT_PATH."/user/");
			} else {
				$this->data['error'] = 'Incorrect login or password';
			}
		}

	}

	public function signup(){
		$this->data['error'] = '';
		if( $_POST && isset($_POST['login']) && isset($_POST['password']) ){
			$login = htmlentities($_POST['login']);
			$password = htmlentities($_POST['password']);
    
			if(strlen($login)<4){
				$this->data['error'] = "Short login";
			}

			if(strlen($login)>=30){
				$this->data['error'] = "long login";
			}

			if(strlen($password)<4){
				$this->data['error'] = "Short password";
				
			}

			if(strlen($password)>30){
				$this->data['error'] = "Long password";
				
			}

			$user = $this->model->getByLogin($login);

			if( !$user ){
				if(($_POST['password'] <=> $_POST["passwordTwo"])!=0){
					$this->data['error'] = "Different passwords";
				} else {
					$result = $this->model->register($login, $password);
					if($result){
						$user = $this->model->getByLogin($_POST['login']);
						Session::set('login', $user['login']);
						Session::set('id', $user['id']);
						Session::set('role', 'user');

						setcookie('login', $user['login'], time()+60*60*24*7); //логин
						setcookie('id', $user['id'], time()+60*60*24*7);
						
						Router::redirect(PROJECT_PATH."/user/");
					}
				}
			} else {
				$this->data['error'] = "Login is used";	
			}
	//return $errors;
	}
}

	public function logout(){
		Session::destroy();
		setcookie('login', '', time()); //удаляем логин
		setcookie('id', '', time()); //удаляем ключ
		Router::redirect(PROJECT_PATH."/");
	}
}