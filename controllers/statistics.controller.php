<?php 

class StatisticsController extends Controller{

	public function __construct($data = array()){
		parent::__construct($data);
		$this->model = new Statistic();
		$this->data['listsize'] = $this->model->getCountPomodoros();
		$this->data['totaltimework'] = $this->model->getStringTotalTimeWork();
		$this->data['countdays'] = $this->model->getCountDays();
	}

	public function user_index(){
		$this->data['pomadoros'] = $this->model->getList();
		$this->data['currentPage'] = 1;
	}

	public function user_view(){
		$params = App::getRouter()->getParams();

		if( isset($params[0]) ){
			$alias = strtolower($params[0]);
			$this->data['pomadoros'] = $this->model->getByAlias($alias);
			$this->data['currentPage'] = $alias;
		}
	}

}