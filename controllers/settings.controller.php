<?php

class SettingsController extends Controller{

	public function __construct($data = array()){
		parent::__construct($data);
		$this->model = new TimeSettings;
	}

	public function index(){
		
	}

	public function user_index(){
		$this->data['settings'] = $this->model->getSettings();
	}

}