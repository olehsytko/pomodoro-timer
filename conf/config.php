<?php


Config::set('site_name', 'Pomadoro Timer');

Config::set('languages', array('en','ru'));

// Routes. Route name => method prefix

Config::set('routes',array(
	'default' => '',
	// 'admin' => 'admin_',
	'user' => 'user_'
));

Config::set('default_route', 'default');
Config::set('default_language', 'en');
Config::set('default_controller', 'timer');
Config::set('default_action', 'index');

Config::set('db.host', 'localhost');
Config::set('db.user', 'root');
Config::set('db.password', '233685614');
Config::set('db.db_name', 'pomadoro_timer');

Config::set('salt', 'gi84kdps95mg31hud1cnkg');